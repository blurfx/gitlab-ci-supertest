import 'mocha';
import request from 'supertest';
import app from './index';

describe('Test suite', () => {
    it('should return http 200', () => {
        return request(app).get('/healthcheck').expect(200);
    })

    it('should return "resolved"', () => {
        return request(app).get('/async').expect('resolved');
    }).timeout(10000);
})

//app.close();