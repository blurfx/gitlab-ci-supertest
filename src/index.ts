import express from 'express';

const app = express();


app.use('/healthcheck', (req, res) => {
    res.sendStatus(200);
})

app.use('/async', async(req, res) => {
    const text = await new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve('resolved');
        }, 2000);
    });
    res.send(text);
});

//const server = app.listen(3000, () => { });

export = app;